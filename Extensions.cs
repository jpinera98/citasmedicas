﻿using AutoMapper;
using ProyectoCitasMedicas.Dtos;
using ProyectoCitasMedicas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas
{
    public static class Extensions
    {

        public static UsuarioDto UserAsDto(this Usuario usuario)
        {
            return new UsuarioDto
            {
                UsuarioId = usuario.UsuarioId,
                Nombre = usuario.Nombre,
                Apellidos = usuario.Apellidos,
                User = usuario.User
            };
        }
        
        /*
        public static MedicoDto MedicoAsDto(this Medico medico)
        {
            MedicoDto mdto = autoMapper.Map<MedicoDto>(medico);

            if (medico.Citas is not null)
            {
                mdto.CitasCitaId = medico.Citas.Select(c => c.CitaId).ToList();
            }

            if (medico.Pacientes is not null)
            {
                mdto.PacientesPacienteId = medico.Pacientes.Select(p => p.UsuarioId).ToList();
            }

            return mdto;
        }*/
    
        /*
        public static PacienteDto PacienteAsDto(this Paciente paciente)
        {
            PacienteDto pdto = new PacienteDto
            {
                UsuarioId = paciente.UsuarioId,
                Nombre = paciente.Nombre,
                Apellidos = paciente.Apellidos,
                User = paciente.User,
                Nss = paciente.Nss,
                NumTarjeta = paciente.NumTarjeta,
                Telefono = paciente.Telefono,
                Direccion = paciente.Direccion,
                Citas = new List<int>(),
                Medicos = new List<int>()
            };

            if (paciente.Citas is not null)
            {
                pdto.Citas = paciente.Citas.Select(c => c.CitaId).ToList();
            }

            if (paciente.Medicos is not null)
            {
                pdto.Medicos = paciente.Medicos.Select(m => m.UsuarioId).ToList();
            }

            return pdto;
        } */

        public static CitaDto CitaAsDto(this Cita cita)
        {
            var dto = new CitaDto
            {
                CitaId = cita.CitaId,
                MotivoCita = cita.MotivoCita,
                MedicoUsuarioId = cita.MedicoForeignKey,
                PacienteUsuarioId = cita.PacienteForeignKey
            };

            if(cita.Diagnostico is not null)
            {
                dto.DiagnosticoDiagnosticoId = cita.Diagnostico.DiagnosticoId;
            }

            return dto;
        }

        public static DiagnosticoDto DiagnosticoAsDto(this Diagnostico diagnostico)
        {
            return new DiagnosticoDto
            {
                DiagnosticoId = diagnostico.DiagnosticoId,
                ValEspecialista = diagnostico.ValEspecialista,
                Enfermedad = diagnostico.Enfermedad,
                CitaId = diagnostico.CitaId
            };
        }

    }
}
