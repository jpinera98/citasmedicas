﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProyectoCitasMedicas.Migrations
{
    public partial class nuevaMigracion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Medicos_Pacientes_PacienteUsuarioId",
                table: "Medicos");

            migrationBuilder.DropIndex(
                name: "IX_Medicos_PacienteUsuarioId",
                table: "Medicos");

            migrationBuilder.DropColumn(
                name: "PacienteUsuarioId",
                table: "Medicos");

            migrationBuilder.CreateTable(
                name: "MedicoPaciente",
                columns: table => new
                {
                    MedicosUsuarioId = table.Column<int>(type: "NUMBER(10)", nullable: false),
                    PacientesUsuarioId = table.Column<int>(type: "NUMBER(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MedicoPaciente", x => new { x.MedicosUsuarioId, x.PacientesUsuarioId });
                    table.ForeignKey(
                        name: "FK_MedicoPaciente_Medicos_MedicosUsuarioId",
                        column: x => x.MedicosUsuarioId,
                        principalTable: "Medicos",
                        principalColumn: "UsuarioId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MedicoPaciente_Pacientes_PacientesUsuarioId",
                        column: x => x.PacientesUsuarioId,
                        principalTable: "Pacientes",
                        principalColumn: "UsuarioId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MedicoPaciente_PacientesUsuarioId",
                table: "MedicoPaciente",
                column: "PacientesUsuarioId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MedicoPaciente");

            migrationBuilder.AddColumn<int>(
                name: "PacienteUsuarioId",
                table: "Medicos",
                type: "NUMBER(10)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Medicos_PacienteUsuarioId",
                table: "Medicos",
                column: "PacienteUsuarioId");

            migrationBuilder.AddForeignKey(
                name: "FK_Medicos_Pacientes_PacienteUsuarioId",
                table: "Medicos",
                column: "PacienteUsuarioId",
                principalTable: "Pacientes",
                principalColumn: "UsuarioId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
