﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProyectoCitasMedicas.Migrations
{
    public partial class nuevaMigracion2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "PacienteForeignKey",
                table: "citas",
                type: "NUMBER(10)",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PacienteForeignKey",
                table: "citas");
        }
    }
}
