﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProyectoCitasMedicas.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Usuarios",
                columns: table => new
                {
                    UsuarioId = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    Nombre = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    Apellidos = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    User = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    Clave = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuarios", x => x.UsuarioId);
                });

            migrationBuilder.CreateTable(
                name: "Pacientes",
                columns: table => new
                {
                    UsuarioId = table.Column<int>(type: "NUMBER(10)", nullable: false),
                    Nss = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    NumTarjeta = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    Telefono = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
                    Direccion = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pacientes", x => x.UsuarioId);
                    table.ForeignKey(
                        name: "FK_Pacientes_Usuarios_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "Usuarios",
                        principalColumn: "UsuarioId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Medicos",
                columns: table => new
                {
                    UsuarioId = table.Column<int>(type: "NUMBER(10)", nullable: false),
                    NumColegiado = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    PacienteUsuarioId = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medicos", x => x.UsuarioId);
                    table.ForeignKey(
                        name: "FK_Medicos_Pacientes_PacienteUsuarioId",
                        column: x => x.PacienteUsuarioId,
                        principalTable: "Pacientes",
                        principalColumn: "UsuarioId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Medicos_Usuarios_UsuarioId",
                        column: x => x.UsuarioId,
                        principalTable: "Usuarios",
                        principalColumn: "UsuarioId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "citas",
                columns: table => new
                {
                    CitaId = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    FechaHora = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false),
                    MotivoCita = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    MedicoForeignKey = table.Column<int>(type: "NUMBER(10)", nullable: false),
                    PacienteUsuarioId = table.Column<int>(type: "NUMBER(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_citas", x => x.CitaId);
                    table.ForeignKey(
                        name: "FK_citas_Medicos_MedicoForeignKey",
                        column: x => x.MedicoForeignKey,
                        principalTable: "Medicos",
                        principalColumn: "UsuarioId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_citas_Pacientes_PacienteUsuarioId",
                        column: x => x.PacienteUsuarioId,
                        principalTable: "Pacientes",
                        principalColumn: "UsuarioId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "diagnosticos",
                columns: table => new
                {
                    DiagnosticoId = table.Column<int>(type: "NUMBER(10)", nullable: false)
                        .Annotation("Oracle:Identity", "1, 1"),
                    ValEspecialista = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    Enfermedad = table.Column<string>(type: "NVARCHAR2(2000)", nullable: false),
                    CitaId = table.Column<int>(type: "NUMBER(10)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_diagnosticos", x => x.DiagnosticoId);
                    table.ForeignKey(
                        name: "FK_diagnosticos_citas_CitaId",
                        column: x => x.CitaId,
                        principalTable: "citas",
                        principalColumn: "CitaId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_citas_MedicoForeignKey",
                table: "citas",
                column: "MedicoForeignKey");

            migrationBuilder.CreateIndex(
                name: "IX_citas_PacienteUsuarioId",
                table: "citas",
                column: "PacienteUsuarioId");

            migrationBuilder.CreateIndex(
                name: "IX_diagnosticos_CitaId",
                table: "diagnosticos",
                column: "CitaId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Medicos_PacienteUsuarioId",
                table: "Medicos",
                column: "PacienteUsuarioId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "diagnosticos");

            migrationBuilder.DropTable(
                name: "citas");

            migrationBuilder.DropTable(
                name: "Medicos");

            migrationBuilder.DropTable(
                name: "Pacientes");

            migrationBuilder.DropTable(
                name: "Usuarios");
        }
    }
}
