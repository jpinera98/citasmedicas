﻿using Microsoft.AspNetCore.Mvc;
using ProyectoCitasMedicas.Dtos;
using ProyectoCitasMedicas.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Controllers
{
    [Route("api/[controller]")]
    public class PacientesController : ControllerBase
    {
        private readonly PacienteService service;

        public PacientesController(PacienteService service)
        {
            this.service = service;
        }

        [HttpGet]
        public async Task<IEnumerable<PacienteDto>> GetPacientes()
        {
            return await service.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PacienteDto>> GetPaciente(int id)
        {
            var pacienteDto = await service.GetPaciente(id);

            if (pacienteDto is null)
            {
                return NotFound();
            }

            return pacienteDto;
        }

        [HttpPost]
        public async Task<ActionResult<PacienteDto>> CreatePaciente(CreatePacienteDto pacienteDto)
        {
            PacienteDto pdto = await service.AddPaciente(pacienteDto);

            return CreatedAtAction(nameof(GetPaciente), new { id = pdto.UsuarioId }, pacienteDto);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdatePaciente(PacienteDto pacienteDto)
        {
            if (await service.UpdatePaciente(pacienteDto))
            {
                return NoContent();
            }

            return NotFound();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePaciente(int id)
        {
            if (await service.DeletePaciente(id))
            {
                return NoContent();
            }

            return NotFound();
        }

    }
}
