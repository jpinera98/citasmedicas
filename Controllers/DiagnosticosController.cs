﻿using Microsoft.AspNetCore.Mvc;
using ProyectoCitasMedicas.Dtos;
using ProyectoCitasMedicas.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class DiagnosticosController : ControllerBase
    {

        private readonly DiagnosticoService service;

        public DiagnosticosController(DiagnosticoService service)
        {
            this.service = service;
        }

        [HttpGet]
        public async Task<IEnumerable<DiagnosticoDto>> GetDiagnosticos()
        {
            return await service.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<DiagnosticoDto>> GetDiagnostico(int id)
        {
            var diagnosticoDto = await service.GetDiagnostico(id);

            if (diagnosticoDto is null)
            {
                return NotFound();
            }

            return diagnosticoDto;
        }



        [HttpPost]
        public async Task<ActionResult<DiagnosticoDto>> CreateDiagnostico(CreateDiagnosticoDto diagnosticoDto)
        {

            DiagnosticoDto ddto = await service.AddDiagnostico(diagnosticoDto);
            if (ddto is null)
            {
                return NotFound();
            }

            return CreatedAtAction(nameof(GetDiagnostico), new { id = ddto.DiagnosticoId }, diagnosticoDto);
        }


        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateDiagnostico(DiagnosticoDto diagnosticoDto)
        {
            if (await service.UpdateDiagnostico(diagnosticoDto))
            {
                return NoContent();
            }

            return NotFound();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteDiagnostico(int id)
        {
            if (await service.DeleteDiagnostico(id))
            {
                return NoContent();
            }

            return NotFound();
        }





    }
}
