﻿using Microsoft.AspNetCore.Mvc;
using ProyectoCitasMedicas.Dtos;
using ProyectoCitasMedicas.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitasController : ControllerBase
    {
        private readonly CitaService service;

        public CitasController(CitaService service)
        {
            this.service = service;
        }

        [HttpGet]
        public async Task<IEnumerable<CitaDto>> GetCitas()
        {
            return await service.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CitaDto>> GetCita(int id)
        {
            var citaDto =  await service.GetCita(id);

            if (citaDto is null)
            {
                return NotFound();
            }

            return citaDto;
        }

        [HttpPost]
        public async Task<ActionResult<CitaDto>> CreateCita(CreateCitaDto citaDto)
        {
            
            CitaDto cdto = await service.AddCita(citaDto);
            if(cdto is null)
            {
                return NotFound();
            }

            return CreatedAtAction(nameof(GetCita), new { id = cdto.CitaId }, citaDto);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateCita(CitaDto citaDto)
        {
            if (await service.UpdateCita(citaDto))
            {
                return NoContent();
            }

            return NotFound();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteCita(int id)
        {
            if (await service.DeleteCita(id))
            {
                return NoContent();
            }

            return NotFound();
        }


    }
}
