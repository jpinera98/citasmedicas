﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProyectoCitasMedicas;
using ProyectoCitasMedicas.Dtos;
using ProyectoCitasMedicas.Services;

namespace ProyectoCitasMedicas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly UsuarioService service;

        public UsuariosController(UsuarioService service)
        {
            this.service = service;
        }

        // GET: api/Usuarios
        [HttpGet]
        public IEnumerable<UsuarioDto> GetUsuarios()
        {
            return service.GetAll();
        }


        [HttpGet("{id}")]
        public ActionResult<UsuarioDto> GetUsuario(int id)
        {
            var usuarioDto = service.GetUsuario(id);

            if(usuarioDto is null)
            {
                return NotFound();
            }

            return usuarioDto;
        }

        [HttpPost]
        public ActionResult<UsuarioDto> CreateUsuario(CreateUsuarioDto usuarioDto)
        {
            UsuarioDto udto = service.AddUser(usuarioDto);

            return CreatedAtAction(nameof(GetUsuario), new { id = udto.UsuarioId }, usuarioDto);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateUsuario(UsuarioDto usuarioDto)
        {
            if (await service.UpdateUser(usuarioDto))
            {
                return NoContent();
            }

            return NotFound();
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteUser(int id)
        {
            if (service.DeleteUser(id))
            {
                return NoContent();
            }

            return NotFound();
        }





        

        /*

        // PUT: api/Usuarios/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUsuario(int id, Usuario usuario)
        {
            if (id != usuario.UsuarioId)
            {
                return BadRequest();
            }

            _context.Entry(usuario).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        */

        /*

        // POST: api/Usuarios
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Usuario>> PostUsuario(Usuario usuario)
        {
            _context.Usuarios.Add(usuario);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUsuario", new { id = usuario.UsuarioId }, usuario);
        }
        */

        /*
        // DELETE: api/Usuarios/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUsuario(int id)
        {
            var usuario = await _context.Usuarios.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }

            _context.Usuarios.Remove(usuario);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        */

        /*
        private bool UsuarioExists(int id)
        {
            return _context.Usuarios.Any(e => e.UsuarioId == id);
        }
        */
    }
}
