﻿using Microsoft.AspNetCore.Mvc;
using ProyectoCitasMedicas.Dtos;
using ProyectoCitasMedicas.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Controllers
{
    [Route("api/[controller]")]
    public class MedicosController : ControllerBase
    {
        private readonly MedicoService service;

        public MedicosController(MedicoService service)
        {
            this.service = service;
        }

        [HttpGet]
        public async Task<IEnumerable<MedicoDto>> GetMedicos()
        {
            return await service.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<MedicoDto>> GetMedico(int id)
        {
            var medicoDto = await service.GetMedico(id);

            if (medicoDto is null)
            {
                return NotFound();
            }

            return medicoDto;
        }

        [HttpPost]
        public async Task<ActionResult<MedicoDto>> CreateMedico(CreateMedicoDto medicoDto)
        {
            MedicoDto mdto = await service.AddMedico(medicoDto);

            return CreatedAtAction(nameof(GetMedico), new { id = mdto.UsuarioId }, medicoDto);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateMedico(int id, CreateMedicoDto medicoDto)
        {
            if (await service.UpdateMedico(id, medicoDto))
            {
                return NoContent();
            }

            return NotFound();
        }


        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteMedico(int id)
        {
            if (await service.DeleteMedico(id))
            {
                return NoContent();
            }

            return NotFound();
        }


    }
}
