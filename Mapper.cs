﻿using AutoMapper;
using ProyectoCitasMedicas.Dtos;
using ProyectoCitasMedicas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas
{
    public class Mapper : Profile
    {
        public Mapper()
        {
            CreateMap<Diagnostico, DiagnosticoDto>();
            CreateMap<DiagnosticoDto, Diagnostico>();
            CreateMap<Cita, CitaDto>();
            CreateMap<CitaDto, Cita>();
            CreateMap<Paciente, PacienteDto>();
            CreateMap<PacienteDto, Paciente>();
            CreateMap<Usuario, UsuarioDto>();
            CreateMap<UsuarioDto, Usuario>();
            CreateMap<Medico, MedicoDto>();
            CreateMap<MedicoDto, Medico>();
        }



       
    }
}
