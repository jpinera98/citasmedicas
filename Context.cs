﻿using Microsoft.EntityFrameworkCore;
using ProyectoCitasMedicas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas
{
    public class Context : DbContext
    {

        public Context(DbContextOptions<Context> options) : base(options)
        {

        }

        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<Paciente> Pacientes { get; set; }

        public DbSet<Medico> Medicos { get; set; }

        public DbSet<Cita> Citas  { get; set; }

        public DbSet<Diagnostico> Diagnosticos  { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /*
            modelBuilder.HasSequence<int>("SeqUsuario");
            modelBuilder.Entity<Usuario>()
                .Property(u => u.UsuarioId)
                .HasDefaultValueSql("NEXT VALUE FOR dotnet5.SeqUsuario");
            */

            
            modelBuilder.Entity<Usuario>().ToTable("Usuarios");
            modelBuilder.Entity<Paciente>().ToTable("Pacientes");
            modelBuilder.Entity<Medico>()
                .ToTable("Medicos")
                .HasMany(c => c.Citas)
                .WithOne(m => m.Medico);

            modelBuilder.Entity<Cita>()
                .HasOne(c => c.Medico)
                .WithMany(m => m.Citas)
                .HasForeignKey(m => m.MedicoForeignKey);

            /*modelBuilder.Entity<MedicoPaciente>().HasKey(x => new { x.MedicoId, x.PacienteId });
            


            modelBuilder.Entity<MedicoPaciente>()
                .HasKey(c => new { c.MedicoId, c.PacienteId });

            modelBuilder.Entity<Medico>()
                .ToTable("medicos")
                .HasMany(x => x.Citas)
                .WithOne(x => x.Medico)
                .HasForeignKey(x => x.MedicoUsuarioId);

            modelBuilder.Entity<Cita>()
                .ToTable("citas")
                .HasOne(d => d.Medico)
                .WithMany(d => d.Citas)
                .HasForeignKey(e => e.MedicoUsuarioId); */
        }


    }
 
}
