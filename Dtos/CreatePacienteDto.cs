﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Dtos
{
    public class CreatePacienteDto
    {

        public string Nombre { get; set; }

        public string Apellidos { get; set; }

        public string User { get; set; }

        public string Clave { get; set; }

        public string Nss { get; set; }

        public string NumTarjeta { get; set; }

        public string Telefono { get; set; }

        public string Direccion { get; set; }

        public ICollection<int> Medicos { get; set; }

        public ICollection<int> Citas { get; set; }

    }
}
