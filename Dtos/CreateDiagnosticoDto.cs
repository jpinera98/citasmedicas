﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Dtos
{
    public class CreateDiagnosticoDto
    {

        public string ValEspecialista { get; set; }

        public string Enfermedad { get; set; }

        public int CitaId { get; set; }
    }
}
