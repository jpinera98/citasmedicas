﻿using ProyectoCitasMedicas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Dtos
{
    public class MedicoDto
    {
        public int UsuarioId { get; set; }

        public string Nombre { get; set; }

        public string Apellidos { get; set; }

        public string User { get; set; }

        public string NumColegiado { get; set; }

        public ICollection<int> CitasCitaId { get; set; }

        public ICollection<int> PacientesUsuarioId { get; set; }
    }
}
