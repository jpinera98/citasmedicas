﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Dtos
{
    public class CreateCitaDto
    {
        public string MotivoCita { get; set; }

        public int MedicoId { get; set; }

        public int PacienteId { get; set; }

    }
}
