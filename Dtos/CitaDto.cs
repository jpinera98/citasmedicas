﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Dtos
{
    public class CitaDto
    {
        public int CitaId { get; set; }

        public string MotivoCita { get; set; }

        public int MedicoUsuarioId { get; set; }

        public int PacienteUsuarioId { get; set; }

        public int DiagnosticoDiagnosticoId { get; set; }
    }
}
