﻿using ProyectoCitasMedicas.Dtos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Entities
{
    [Table ("medicos")]
    public class Medico : Usuario
    {
        

        [Required]
        public string NumColegiado { get; set; }

        public ICollection<Cita> Citas { get; set; } = new List<Cita>();

        public ICollection<Paciente> Pacientes { get; set; } = new List<Paciente>();
        

    }
}

