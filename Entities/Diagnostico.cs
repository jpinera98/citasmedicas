﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Entities
{
    [Table ("diagnosticos")]
    public class Diagnostico
    {
        public int DiagnosticoId { get; set; }

        [Required]
        public string ValEspecialista { get; set; }

        [Required]
        public string Enfermedad { get; set; }


        public int CitaId { get; set; }

        [Required]
        public Cita Cita { get; set; }
    }
}
