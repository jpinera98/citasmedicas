﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Entities
{
    [Table ("pacientes")]
    public class Paciente : Usuario
    {
        [Required]
        public string Nss { get; set; }
        
        [Required]
        public string NumTarjeta { get; set; }

        public string Telefono { get; set; }

        [Required]
        public string Direccion { get; set; }

        public ICollection<Medico> Medicos { get; set; } = new List<Medico>();

        public ICollection<Cita> Citas { get; set; } = new List<Cita>();

       


    }
}
