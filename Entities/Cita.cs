﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Entities
{
    [Table ("citas")]
    public class Cita
    {
        public int CitaId { get; set; }

        public DateTime FechaHora { get; set; }

        [Required]
        public string MotivoCita { get; set; }

        public int MedicoForeignKey { get; set; }

        public Medico Medico { get; set; }

        public int PacienteForeignKey{ get; set; }
        
        public Paciente Paciente { get; set; }

        public Diagnostico Diagnostico { get; set; }


    }
}
