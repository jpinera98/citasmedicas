﻿using Microsoft.EntityFrameworkCore;
using ProyectoCitasMedicas.Dtos;
using ProyectoCitasMedicas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace ProyectoCitasMedicas.Services
{
    public class DiagnosticoService
    {

        private readonly Context context;
        private readonly IMapper autoMapper;

        public DiagnosticoService (Context context, IMapper autoMapper)
        {
            this.context = context;
            this.autoMapper = autoMapper;
        }

        public async Task<IEnumerable<DiagnosticoDto>> GetAll()
        {
            IEnumerable<Diagnostico> list = await context.Diagnosticos.ToListAsync();

            return list.Select(d => autoMapper.Map<DiagnosticoDto>(d));

            //return list.Select(d => d.DiagnosticoAsDto());
        }

        public async Task<DiagnosticoDto> GetDiagnostico(int id)
        {
            var diagnostico = await context.Diagnosticos.FindAsync(id);

            if (diagnostico is null)
            {
                return null;
            }

            return autoMapper.Map<DiagnosticoDto>(diagnostico);
        }

        public async Task<DiagnosticoDto> AddDiagnostico(CreateDiagnosticoDto diagnosticoDto)
        {
            Cita cit = await context.Citas.FindAsync(diagnosticoDto.CitaId);

            if (cit is null)
            {
                return null;
            }

            Diagnostico diagnostico = new()
            {
                ValEspecialista = diagnosticoDto.ValEspecialista,
                Enfermedad = diagnosticoDto.Enfermedad,
                CitaId = diagnosticoDto.CitaId
            };

            cit.Diagnostico = diagnostico;
            context.Entry(cit).State = EntityState.Modified;
            //await context.Diagnosticos.AddAsync(diagnostico);
            await context.SaveChangesAsync();

            return autoMapper.Map<DiagnosticoDto>(diagnostico);
            
            //return diagnostico.DiagnosticoAsDto();
        }


        public async Task<bool> UpdateDiagnostico(DiagnosticoDto diagnosticoDto)
        {
            if (!await context.Diagnosticos.AnyAsync(d => d.DiagnosticoId == diagnosticoDto.DiagnosticoId))
            {
                return false;
            }

            if (!await context.Citas.AnyAsync(c => c.CitaId == diagnosticoDto.CitaId))
            {
                return false;
            }


            Diagnostico diagnostico = autoMapper.Map<Diagnostico>(diagnosticoDto);
            context.Entry(diagnostico).State = EntityState.Modified;
            context.SaveChanges();

            return true;
        }


        public async Task<bool> DeleteDiagnostico(int id)
        {
            Diagnostico diagnostico = await context.Diagnosticos.FindAsync(id);
            if (diagnostico is null)
            {
                return false;
            }

            context.Remove(diagnostico);
            await context.SaveChangesAsync();

            return true;
        }









    }
}
