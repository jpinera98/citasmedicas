﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProyectoCitasMedicas.Dtos;
using ProyectoCitasMedicas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Services
{
    public class PacienteService
    {

        private readonly Context context;
        private readonly IMapper autoMapper;

        public PacienteService(Context context, IMapper autoMapper)
        {
            this.context = context;
            this.autoMapper = autoMapper;
        }

        public async Task<IEnumerable<PacienteDto>> GetAll()
        {
            IEnumerable<Paciente> list = await context.Pacientes.Include(p => p.Citas).Include(p => p.Medicos).ToListAsync();

            return list.Select(p => PacienteAsDto(p));
            //return list.Select(p => p.PacienteAsDto());
        }

        public async Task<PacienteDto> GetPaciente(int id)
        {
            var paciente = await context.Pacientes.FindAsync(id);
            if (paciente is null)
            {
                return null;
            }

            return PacienteAsDto(paciente);
        }

        public async Task<PacienteDto> AddPaciente(CreatePacienteDto pacienteDto)
        {

            ICollection<int> medicos = pacienteDto.Medicos;
            ICollection<Medico> meds = new List<Medico>();

            foreach (int medico in medicos)
            {
                Medico med = await context.Medicos.FindAsync(medico);
                if (med is null)
                {
                    return null;
                }
                else
                {
                    meds.Add(med);
                }
            }

            Paciente paciente = new()
            {
                Nombre = pacienteDto.Nombre,
                Apellidos = pacienteDto.Apellidos,
                Clave = pacienteDto.Clave,
                User = pacienteDto.User,
                Nss = pacienteDto.Nss,
                NumTarjeta = pacienteDto.NumTarjeta,
                Telefono = pacienteDto.Telefono,
                Direccion = pacienteDto.Direccion,
                Medicos = meds
            };

            foreach (Medico medico in meds)
            {
                medico.Pacientes.Add(paciente);
                context.Entry(medico).State = EntityState.Modified;
            }

            await context.SaveChangesAsync();
            return PacienteAsDto(paciente);
        }

        public async Task<bool> UpdatePaciente(PacienteDto pacienteDto)
        {

            Paciente aux = await context.Pacientes.Include(p => p.Medicos).SingleAsync(p => p.UsuarioId == pacienteDto.UsuarioId);

            if(aux == null)
            {
                return false;
            }

            /*
            if (!await context.Pacientes.AnyAsync(p => p.UsuarioId == pacienteDto.UsuarioId))
            {
                return false;
            }*/



            Paciente paciente = PacienteDtoToEntity(pacienteDto);
            if (paciente is null)
            {
                return false;
            }

            foreach (Medico med in paciente.Medicos)
            {
                if (aux.Medicos.Contains(med))
                {
                    paciente.Medicos.Remove(med);
                }
            }

            context.Entry(paciente).State = EntityState.Modified;
            context.SaveChanges();

            return true;
        }

        public async Task<bool> DeletePaciente(int id)
        {
            Paciente paciente = await context.Pacientes.FindAsync(id);
            if (paciente is null)
            {
                return false;
            }

            context.Remove(paciente);
            await context.SaveChangesAsync();

            return true;
        }


        private PacienteDto PacienteAsDto(Paciente paciente)
        {
            PacienteDto pdto = autoMapper.Map<PacienteDto>(paciente);

            if (paciente.Citas is not null)
            {
                pdto.CitasCitaId = paciente.Citas.Select(c => c.CitaId).ToList();
            }

            if (paciente.Medicos is not null)
            {
                pdto.MedicosUsuarioId = paciente.Medicos.Select(m => m.UsuarioId).ToList();
            }

            return pdto;
        }

        private Paciente PacienteDtoToEntity(PacienteDto pdto)
        {
            Paciente pac = autoMapper.Map<Paciente>(pdto);

            List<Medico> medicos = new List<Medico>();
            foreach (int med in pdto.MedicosUsuarioId)
            {
                Medico medico = context.Medicos.Find(med);
                if(medico is null)
                {
                    return null;
                }
            }

            pac.Medicos = medicos;

            return pac;

        }
    }
}
