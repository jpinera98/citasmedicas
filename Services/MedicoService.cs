﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProyectoCitasMedicas.Dtos;
using ProyectoCitasMedicas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Services
{

    public class MedicoService
    {

        private readonly Context context;
        private readonly IMapper autoMapper;

        public MedicoService(Context context, IMapper autoMapper)
        {
            this.context = context;
            this.autoMapper = autoMapper;
        }

        public async Task<IEnumerable<MedicoDto>> GetAll()
        {
            IEnumerable<Medico> list = await context.Medicos.Include(m => m.Pacientes).Include(m => m.Citas).ToListAsync();

            return list.Select(m => MedicoAsDto(m));

            //return list.Select(m => m.MedicoAsDto());
        }

        public async Task<MedicoDto> GetMedico(int id)
        {
            var medico = await context.Medicos.Include(m => m.Citas).Include(m => m.Pacientes).SingleAsync(m => m.UsuarioId == id);
            if (medico is null)
            {
                return null;
            }

            return MedicoAsDto(medico);

            //return medico.MedicoAsDto();
        }

        public async Task<MedicoDto> AddMedico(CreateMedicoDto medicoDto)
        {

            Medico medico = new()
            {
                Nombre = medicoDto.Nombre,
                Apellidos = medicoDto.Apellidos,
                Clave = medicoDto.Clave,
                User = medicoDto.User,
                NumColegiado = medicoDto.NumColegiado,
                Citas = new List<Cita>()
            };

            //medico.Citas = medicoDto.Citas.Select(c => context.Citas.Find(c)).ToList();
      
            await context.Medicos.AddAsync(medico);
            await context.SaveChangesAsync();

            return autoMapper.Map<MedicoDto>(medico);
            //return medico.MedicoAsDto();
        }


        public async Task<bool> UpdateMedico(int id, CreateMedicoDto medicoDto)
        {
            Medico medico = await context.Medicos.FindAsync(id);

            if (medico is null)
            {
                return false;
            }

            medico.Nombre = medicoDto.Nombre;
            medico.Apellidos = medicoDto.Apellidos;
            medico.Clave = medicoDto.Clave;
            medico.User = medicoDto.User;
            medico.NumColegiado = medicoDto.NumColegiado;
            context.Update(medico);
            await context.SaveChangesAsync();

            return true;
        }


        public async Task<bool> DeleteMedico(int id)
        {
            Medico medico = await context.Medicos.FindAsync(id);
            if (medico is null)
            {
                return false;
            }

            context.Remove(medico);
            await context.SaveChangesAsync();

            return true;
        }


        private MedicoDto MedicoAsDto(Medico medico)
        {
            MedicoDto mdto = autoMapper.Map<MedicoDto>(medico);

            if (medico.Citas is not null)
            {
                mdto.CitasCitaId = medico.Citas.Select(c => c.CitaId).ToList();
            }

            if (medico.Pacientes is not null)
            {
                mdto.PacientesUsuarioId = medico.Pacientes.Select(p => p.UsuarioId).ToList();
            }

            return mdto;
        }



    }
}
