﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProyectoCitasMedicas.Dtos;
using ProyectoCitasMedicas.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Services
{
    public class CitaService
    {

        private readonly Context context;
        private readonly IMapper autoMapper;

        public CitaService (Context context, IMapper autoMapper)
        {
            this.context = context;
            this.autoMapper = autoMapper;
        }

        public async Task<IEnumerable<CitaDto>> GetAll()
        {
            IEnumerable<Cita> list = await context.Citas.Include(c => c.Diagnostico).Include(c => c.Medico).Include(c => c.Paciente).ToListAsync();

            return autoMapper.Map<List<CitaDto>>(list);
            //return list.Select(cita => autoMapper.Map<CitaDto>(cita));
        }

        public async Task<CitaDto> GetCita(int id)
        {
            var cita = await context.Citas.Include(c => c.Medico).Include(c => c.Paciente).Include(c => c.Diagnostico).SingleAsync(c => c.CitaId == id);
            
            if (cita is null)
            {
                return null;
            }

            return autoMapper.Map<CitaDto>(cita);
            //return cita.CitaAsDto();
        }

        public async  Task<CitaDto> AddCita(CreateCitaDto citaDto)
        {
            Medico med = await context.Medicos.FindAsync(citaDto.MedicoId);

            if(med is null)
            {
                return null;
            }

            Paciente pac = await context.Pacientes.FindAsync(citaDto.PacienteId);

            if(pac is null)
            {
                return null;
            }

            Cita cita = new()
            {
                MotivoCita = citaDto.MotivoCita,
                MedicoForeignKey = citaDto.MedicoId,
                Medico = med,
                PacienteForeignKey = citaDto.PacienteId,
                Paciente = pac
            };

            pac.Citas.Add(cita);
            med.Citas.Add(cita);
            context.Entry(pac).State = EntityState.Modified;
            context.Entry(med).State = EntityState.Modified;
            //context.Medicos.Update(med);
            //await context.Citas.AddAsync(cita);
            await context.SaveChangesAsync();


            return autoMapper.Map<CitaDto>(cita);
            //return cita.CitaAsDto();
        }

        public async Task<bool> UpdateCita(CitaDto citaDto)
        {
            if(!await context.Citas.AnyAsync(c => c.CitaId == citaDto.CitaId))
            {
                return false;
            }

            Cita cita = autoMapper.Map<Cita>(citaDto);
            context.Entry(cita).State = EntityState.Modified;
            await context.SaveChangesAsync();

            return true;
        }


        public async Task<bool> DeleteCita(int id)
        {
            Cita cita = await context.Citas.FindAsync(id);
            if (cita is null)
            {
                return false;
            }

            context.Remove(cita);
            await context.SaveChangesAsync();

            return true;
        }


    }
}
