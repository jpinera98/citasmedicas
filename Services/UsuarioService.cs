﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProyectoCitasMedicas.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoCitasMedicas.Services
{
    public class UsuarioService
    {
        private readonly Context context;
        private readonly IMapper autoMapper;

        public UsuarioService(Context context, IMapper autoMapper)
        {
            this.context = context;
            this.autoMapper = autoMapper;
        }

        public IEnumerable<UsuarioDto> GetAll()
        {
            //return context.Usuarios.ToList().Select(usuario => usuario.UserAsDto());
            return autoMapper.Map<List<UsuarioDto>>(context.Usuarios);
        }

        public UsuarioDto GetUsuario(int id)
        {
            var usuario = context.Usuarios.Find(id);
            if(usuario is null)
            {
                return null;
            }

            return autoMapper.Map<UsuarioDto>(usuario);
        }

        public UsuarioDto AddUser(CreateUsuarioDto usuarioDto)
        {
            Usuario usuario = new()
            {
                Nombre = usuarioDto.Nombre,
                Apellidos = usuarioDto.Apellidos,
                Clave = usuarioDto.Clave,
                User = usuarioDto.User
            };

            context.Usuarios.Add(usuario);
            context.SaveChanges();
            return autoMapper.Map<UsuarioDto>(usuario);
        }


        public async Task<bool> UpdateUser(UsuarioDto usuarioDto)
        {
            if(!await context.Usuarios.AnyAsync(u => u.UsuarioId == usuarioDto.UsuarioId))
            {
                return false;
            }

            Usuario user = autoMapper.Map<Usuario>(usuarioDto);
            context.Entry(user).State = EntityState.Modified;
            context.SaveChanges();
            /*
            user.Nombre = usuarioDto.Nombre;
            user.Apellidos = usuarioDto.Apellidos;
            user.Clave = usuarioDto.Clave;
            user.User = usuarioDto.User;
            context.Update(user);
            context.SaveChanges();
            */

            return true;
        }

        public bool DeleteUser(int id)
        {
            Usuario user = context.Usuarios.Find(id);
            if(user is null)
            {
                return false;
            }

            context.Remove(user);
            context.SaveChanges();

            return true;
        }




    }
}
